import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Think about Zuma Game. You have a row of balls on the table, colored red(R), yellow(Y), blue(B), green(G), and white(W).<br/>
 * You also have several balls in your hand.<br/>
 * Each time, you may choose a ball in your hand, and insert it into the row (including the leftmost place and rightmost place).<br/>
 * Then, if there is a group of 3 or more balls in the same color touching, remove these balls.<br/>
 * Keep doing this until no more balls can be removed.<br/>
 * Find the minimal balls you have to insert to remove all the balls on the table. If you cannot remove all the balls, output -1.<br/>
 * Examples:
 * <pre>
 * Input: "WRRBBW", "RB"
 * Output: -1
 * Explanation: WRRBBW -> WRR[R]BBW -> WBBW -> WBB[B]W -> WW
 * <br/>
 * Input: "WWRRBBWW", "WRBRW"
 * Output: 2
 * Explanation: WWRRBBWW -> WWRR[R]BBWW -> WWBBWW -> WWBB[B]WW -> WWWW -> empty
 * <br/>
 * Input:"G", "GGGGG"
 * Output: 2
 * Explanation: G -> G[G] -> GG[G] -> empty
 * <br/>
 * Input: "RBYYBBRRB", "YRBGB"
 * Output: 3
 * Explanation: RBYYBBRRB -> RBYY[Y]BBRRB -> RBBBRRB -> RRRB -> B -> B[B] -> BB[B] -> empty
 * </pre>
 * Note:<br/>
 * You may assume that the initial row of balls on the table won’t have any 3 or more consecutive balls with the same color.<br/>
 * The number of balls on the table won't exceed 20, and the string represents these balls is called "board" in the input.<br/>
 * The number of balls in your hand won't exceed 5, and the string represents these balls is called "hand" in the input.<br/>
 * Both input strings will be non-empty and only contain characters 'R','Y','B','G','W'.<br/>
 *
 * @author Maksym Bogdanov
 */
public class Q488_Zuma_Game {

    public static void main(String...args) {
        Q488_Zuma_Game solution = new Q488_Zuma_Game();
        System.out.println(solution.findMinStep("WWRRBBWW", "WRBRW"));
        System.out.println(solution.findMinStep("WRRBBW", "RB"));
        System.out.println(solution.findMinStep("G", "GG"));
        System.out.println(solution.findMinStep("RBYYBBRRB", "YRBGB"));
    }

    public int findMinStep(String board, String hand) {
        char[] boardArray = board.toCharArray();
        char[] handArray = hand.toCharArray();
        PriorityQueue<Board> queue = new PriorityQueue<>();
        Board step = getInitialBoard(boardArray, handArray);
        queue.add(step);
        while(!queue.isEmpty()) {
            step = queue.poll();
            Group group = step.first;
            while (group != null) {
                Board nextStep = getNextStep(step, group);
                if (nextStep != null) {
                    if (nextStep.isEmpty()) {
                        return nextStep.minBalls;
                    }
                    queue.add(nextStep);
                }
                group = group.next;
            }
        }
        return -1;
    }

    private Board getInitialBoard(char[] boardArray, char[] handArray) {
        Board step = new Board();
        Group tmp = new Group(boardArray[0], 1);
        step.minBalls = 0;
        step.first = tmp;
        for (int i = 1; i < boardArray.length; i++) {
            if (tmp.color == boardArray[i]) {
                tmp.count++;
            } else {
                tmp.next = new Group(boardArray[i], 1);
                tmp.next.prev = tmp;
                tmp = tmp.next;
            }
        }
        for (char ch : handArray) {
            Integer count = step.handBalls.get(ch);
            if (count == null) {
                count = 0;
            }
            step.handBalls.put(ch, ++count);
        }
        return step;
    }

    private Board getNextStep(Board previousStep, Group group) {
        Board nextStep = new Board();
        nextStep.minBalls = previousStep.minBalls;
        nextStep.handBalls.putAll(previousStep.handBalls);
        Group modifiedGroup = new Group(group.color, group.count);
        nextStep.first = modifiedGroup;
        Group tmpPrev = group.prev;
        Group tmpNext = modifiedGroup;
        while (tmpPrev != null) {
            Group newPrev = new Group(tmpPrev.color, tmpPrev.count);
            newPrev.next = tmpNext;
            tmpNext.prev = newPrev;
            tmpNext = newPrev;
            nextStep.first = newPrev;
            tmpPrev = tmpPrev.prev;
        }
        tmpNext = group.next;
        tmpPrev = modifiedGroup;
        while (tmpNext != null) {
            Group newNext = new Group(tmpNext.color, tmpNext.count);
            tmpPrev.next = newNext;
            newNext.prev = tmpPrev;
            tmpPrev = newNext;
            tmpNext = tmpNext.next;
        }
        Integer handBallCount = nextStep.handBalls.get(modifiedGroup.color);
        if (handBallCount != null) {
            handBallCount--;
            if (handBallCount == 0) {
                nextStep.handBalls.remove(modifiedGroup.color);
            } else {
                nextStep.handBalls.put(modifiedGroup.color, handBallCount);
            }
            modifiedGroup.count++;
            nextStep.minBalls++;
            while (true) {
                if (modifiedGroup.count - 3 >= 0) {
                    //need to remove current group
                    tmpPrev = modifiedGroup.prev;
                    tmpNext = modifiedGroup.next;
                    if (tmpPrev == null) {
                        //group is first
                        nextStep.first = tmpNext;
                        if (tmpNext == null) {
                            break;
                        }
                        modifiedGroup = tmpNext;
                    } else {
                        tmpPrev.next = tmpNext;
                        if (tmpNext != null) {
                            tmpNext.prev = tmpPrev;
                        }
                        modifiedGroup = tmpPrev;
                    }
                    tmpNext = modifiedGroup.next;
                    while (tmpNext != null && tmpNext.color == modifiedGroup.color) {
                        modifiedGroup.count += tmpNext.count;
                        modifiedGroup.next = tmpNext.next;
                        if (tmpNext.next != null) {
                            modifiedGroup.next.prev = modifiedGroup;
                        }
                        tmpNext = tmpNext.next;
                    }
                } else {
                    break;
                }
            }
        } else {
            return null;
        }
        return nextStep;
    }

    private class Group {
        int count;
        char color;
        Group next;
        Group prev;

        public Group(char color, int count) {
            this.count = count;
            this.color = color;
        }
    }

    private class Board implements Comparable<Board> {
        Group first;
        Map<Character, Integer> handBalls = new HashMap<>();
        int minBalls = 0;

        @Override
        public int compareTo(Board o) {
            return Integer.compare(this.minBalls, o.minBalls);
        }

        public boolean isEmpty() {
            return first == null;
        }
    }
}
