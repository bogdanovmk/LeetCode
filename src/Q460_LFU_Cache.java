import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * @author Maksym Bogdanov
 */
public class Q460_LFU_Cache {
    public static final int INITIAL_FREQUENCY = 0;
    public static final int INITIAL_CAPACITY = 0;

    private HashMap<Integer, CacheNode> values;
    private HashMap<Integer, LinkedHashSet<CacheNode>> frequencies;
    private int capacity = INITIAL_CAPACITY;
    private int minFrequency = INITIAL_FREQUENCY;

    public Q460_LFU_Cache(int capacity) {
        this.capacity = capacity;
        if (capacity > INITIAL_CAPACITY) {
            values = new HashMap<>(capacity);
            frequencies = new HashMap<>();
            frequencies.put(INITIAL_FREQUENCY, new LinkedHashSet<>());
        }
    }

    public int get(int key) {
        if (capacity > INITIAL_CAPACITY) {
            CacheNode cacheNode = values.get(key);
            if (cacheNode != null) {
                int frequency = cacheNode.frequency;
                LinkedHashSet<CacheNode> cacheNodeValues = frequencies.get(frequency);
                cacheNodeValues.remove(cacheNode);
                if (minFrequency == frequency && cacheNodeValues.isEmpty()) {
                    minFrequency++;
                }
                frequency++;
                cacheNodeValues = frequencies.get(frequency);
                if (cacheNodeValues == null) {
                    cacheNodeValues = new LinkedHashSet<>();
                    frequencies.put(frequency, cacheNodeValues);
                }
                cacheNode.frequency = frequency;
                cacheNodeValues.add(cacheNode);
                return cacheNode.value;
            }
        }
        return -1;
    }

    public void put(int key, int value) {
        if (capacity > 0) {
            CacheNode cacheNode = values.get(key);
            if (cacheNode == null) {
                if (capacity == values.size()) {
                    LinkedHashSet<CacheNode> cacheNodeValues = frequencies.get(minFrequency);
                    Iterator<CacheNode> iterator = cacheNodeValues.iterator();
                    cacheNode = iterator.next();
                    iterator.remove();
                    values.remove(cacheNode.key);
                }
                cacheNode = new CacheNode();
                cacheNode.value = value;
                cacheNode.key = key;
                cacheNode.frequency = INITIAL_FREQUENCY;
                minFrequency = INITIAL_FREQUENCY;
                LinkedHashSet<CacheNode> cacheNodeValues = frequencies.get(INITIAL_FREQUENCY);
                cacheNodeValues.add(cacheNode);
                values.put(key, cacheNode);
            } else {
                cacheNode.value = value;
                get(key);
            }
        }
    }


    private class CacheNode {
        int frequency;
        int key;
        int value;
    }

    public static void main(String... args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
    }

    private static void test1() {
        System.out.println("### Test #1 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(2);

        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));       // returns 1
        cache.put(3, 3);    // evicts key 2
        System.out.println(cache.get(2));       // returns -1 (not found)
        System.out.println(cache.get(3));       // returns 3.
        cache.put(4, 4);    // evicts key 1.
        System.out.println(cache.get(1));       // returns -1 (not found)
        System.out.println(cache.get(3));       // returns 3
        System.out.println(cache.get(4));       // returns 4
        System.out.println();
    }

    private static void test2() {
        System.out.println("### Test #2 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(3);
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        cache.put(4, 4);
        System.out.println(cache.get(4));
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        cache.put(5, 5);
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));
        System.out.println(cache.get(5));
        System.out.println();
    }

    private static void test3() {
        System.out.println("### Test #3 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(3);
        cache.put(2, 2);
        cache.put(1, 1);
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        cache.put(3, 3);
        cache.put(4, 4);
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        System.out.println(cache.get(4));
        System.out.println();
    }

    private static void test4() {
        System.out.println("### Test #4 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(1);
        cache.put(2, 1);
        System.out.println(cache.get(2));
        cache.put(3, 2);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        System.out.println();
    }

    private static void test5() {
        System.out.println("### Test #5 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(2);
        cache.put(2, 1);
        cache.put(2, 2);
        System.out.println(cache.get(2));
        cache.put(1, 1);
        cache.put(4, 1);
        System.out.println(cache.get(2));
        System.out.println();
    }

    private static void test6() {
        System.out.println("### Test #6 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(2);
        System.out.println(cache.get(2));
        cache.put(2, 6);
        System.out.println(cache.get(1));
        cache.put(1, 5);
        cache.put(1, 2);
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        System.out.println();
    }

    private static void test7() {
        System.out.println("### Test #7 ###");
        Q460_LFU_Cache cache = new Q460_LFU_Cache(10);
        cache.put(10, 13);  // new entry, count = 1,            0 -> 10
        cache.put(3, 17);   // new entry, count = 2,            0 -> 10, 3
        cache.put(6, 11);   // new entry, count = 3,            0 -> 10, 3, 6
        cache.put(10, 5);   // replace entry, count = 3,        0 -> 3, 6, 10
        cache.put(9, 10);   // new entry, count = 4,            0 -> 3, 6, 10, 9
        System.out.println(cache.get(13));      // returns -1
        cache.put(2, 19);   // new entry, count = 5,            0 -> 3, 6, 10, 9
        System.out.println(cache.get(2));       // returns 19,  1 -> 2
        System.out.println(cache.get(3));       // returns 17   0 -> 6, 10, 9
                                                //              1 -> 2, 3
        cache.put(5, 25);   // new entry, count = 6             0 -> 6, 10, 9, 5
        System.out.println(cache.get(8));       // returns -1
        cache.put(9, 22);   // replace entry, count = 6         0 -> 6, 10, 5, 9
        cache.put(5, 5);    // replace entry, count = 6         0 -> 6, 10, 9, 5
        cache.put(1, 30);   // new entry, count = 7             0 -> 6, 10, 9, 5, 1
        System.out.println(cache.get(11));      // returns -1
        cache.put(9, 12);   // replace entry, count = 7         0 -> 6, 10, 5, 1, 9
        System.out.println(cache.get(7));       // returns -1
        System.out.println(cache.get(5));       // returns 5    0 -> 6, 10, 1, 9
                                                //              1 -> 2, 3, 5
        System.out.println(cache.get(8));       // returns -1
        System.out.println(cache.get(9));       // returns 12   0 -> 6, 10, 1
                                                //              1 -> 2, 3, 5, 9
        cache.put(4, 30);   // new entry, count = 8             0 -> 6, 10, 1, 4
        cache.put(9, 3);    // replace entry, count = 8         0 -> 6, 10, 1, 4, 9
                            //                                  1 -> 2, 3, 5
        System.out.println(cache.get(9));       // returns 3    0 -> 6, 10, 1, 4
                                                //              1 -> 2, 3, 5, 9
        System.out.println(cache.get(10));      // returns 5    0 -> 6, 1, 4
                                                //              1 -> 2, 3, 5, 9, 10
        System.out.println(cache.get(10));      // returns 5    0 -> 6, 1, 4
                                                //              1 -> 2, 3, 5, 9
                                                //              2 -> 10
        cache.put(6, 14);   // replace entry, count = 8         0 -> 1, 4, 6
        cache.put(3, 1);    // replace entry, count = 8         0 -> 1, 4, 6, 3
                            //                                  1 -> 2, 5, 9
                            //                                  2 -> 10
        System.out.println(cache.get(3));       // returns 1    0 -> 1, 4, 6
                                                //              1 -> 2, 5, 9, 3
                                                //              2 -> 10
        cache.put(10, 11);  // replace entry, count = 8         0 -> 1, 4, 6, 10
                            //                                  1 -> 2, 5, 9, 3
        System.out.println(cache.get(8));       // returns -1
        cache.put(2, 14);   // replace entry, count = 8         0 -> 1, 4, 6, 10, 2
                            //                                  1 -> 5, 9, 3
        System.out.println(cache.get(1));       // returns 30   0 -> 4, 6, 10, 2
                                                //              1 -> 5, 9, 3, 1
        System.out.println(cache.get(5));       // returns 5    0 -> 4, 6, 10, 2
                                                //              1 -> 9, 3, 1
                                                //              2 -> 5
        System.out.println(cache.get(4));       // returns 30   0 -> 6, 10, 2
                                                //              1 -> 9, 3, 1, 4
                                                //              2 -> 5
        cache.put(11, 4);   // new entry, count = 9             0 -> 6, 10, 2, 11
                            //                                  1 -> 9, 3, 1, 4
                            //                                  2 -> 5
        cache.put(12, 24);  // new entry, count = 10            0 -> 6, 10, 2, 11, 12
                            //                                  1 -> 9, 3, 1, 4
                            //                                  2 -> 5
        cache.put(5, 18);   // replace entry, count = 10        0 -> 6, 10, 2, 11, 12, 5
                            //                                  1 -> 9, 3, 1, 4
        System.out.println(cache.get(13));      // returns -1
        cache.put(7, 23);   // evicts 6, new entry, count = 10  0 -> 10, 2, 11, 12, 5, 7
                            //                                  1 -> 9, 3, 1, 4
        System.out.println(cache.get(8));       // returns -1
        System.out.println(cache.get(12));      // returns 24   0 -> 10, 2, 11, 5, 7
                                                //              1 -> 9, 3, 1, 4, 12
        cache.put(3, 27);   // replace entry, count = 10        0 -> 10, 2, 11, 5, 7, 3
                            //                                  1 -> 9, 1, 4, 12
        cache.put(2, 12);   // replace entry, count = 10        0 -> 10, 11, 5, 7, 3, 2
                            //                                  1 -> 9, 1, 4, 12
        System.out.println(cache.get(5));       // returns 18   0 -> 10, 11, 7, 3, 2
                                                //              1 -> 9, 1, 4, 12, 5
        cache.put(2, 9);    // replace entry, count = 10        0 -> 10, 11, 7, 3, 2
                            //                                  1 -> 9, 1, 4, 12, 5
        cache.put(13, 4);   // evicts 10, new entry, count = 10 0 -> 11, 7, 3, 2, 13
                            //                                  1 -> 9, 1, 4, 12, 5
        cache.put(8, 18);   // evicts 11, new entry, count = 10 0 -> 7, 3, 2, 13, 8
                            //                                  1 -> 9, 1, 4, 12, 5
        cache.put(1, 7);    // replace entry, count = 10        0 -> 7, 3, 2, 13, 8, 1
                            //                                  1 -> 9, 4, 12, 5
        System.out.println(cache.get(6));       // returns 14
        cache.put(9, 29);
        cache.put(8, 21);
        System.out.println(cache.get(5));
        cache.put(6, 30);
        cache.put(1, 12);
        System.out.println(cache.get(10));
        cache.put(4, 15);
        cache.put(7, 22);
        cache.put(11, 26);
        cache.put(8, 17);
        cache.put(9, 29);
        System.out.println(cache.get(5));
        cache.put(3, 4);
        cache.put(11, 30);
        System.out.println(cache.get(12));
        cache.put(4, 29);
        System.out.println(cache.get(3));
        System.out.println(cache.get(9));
        System.out.println(cache.get(6));
        cache.put(3, 4);
        System.out.println(cache.get(1));
        System.out.println(cache.get(10));
        cache.put(3, 29);
        cache.put(10, 28);
        cache.put(1, 20);
        cache.put(11, 13);
        System.out.println(cache.get(3));
        cache.put(3, 12);
        cache.put(3, 8);
        cache.put(10, 9);
        cache.put(3, 26);
        System.out.println(cache.get(8));
        System.out.println(cache.get(7));
        System.out.println(cache.get(5));
        cache.put(13, 17);
        cache.put(2, 27);
        cache.put(11, 15);
        System.out.println(cache.get(12));
        cache.put(9, 19);
        cache.put(2, 15);
        cache.put(3, 16);
        System.out.println(cache.get(1));
        cache.put(12, 17);
        cache.put(9, 1);
        cache.put(6, 19);
        System.out.println(cache.get(4));
        System.out.println(cache.get(5));
        System.out.println(cache.get(5));
        cache.put(8, 1);
        cache.put(11, 7);
        cache.put(5, 2);
        cache.put(9, 28);
        System.out.println(cache.get(1));
        cache.put(2, 2);
        cache.put(7, 4);
        cache.put(4, 22);
        cache.put(7, 24);
        cache.put(9, 26);
        cache.put(13, 28);
        cache.put(11, 26);
        System.out.println();
    }
}
