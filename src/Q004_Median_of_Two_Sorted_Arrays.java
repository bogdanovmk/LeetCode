/**
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.<br/>
 * Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
 * <p>
 * Example 1:<br/>
 *      nums1 = [1, 3]<br/>
 *      nums2 = [2]<br/>
 *      The median is 2.0
 * </p>
 * <p>
 *     Example 2:<br/>
 *     nums1 = [1, 2]<br/>
 *     nums2 = [3, 4]<br/>
 *     The median is (2 + 3)/2 = 2.5<br/>
 * </p>
 * @author Maksym Bogdanov
 */
public class Q004_Median_of_Two_Sorted_Arrays {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        if (nums1 == null && nums2 == null) {
            throw new IllegalArgumentException();
        }
        int combinedLength = nums1.length + nums2.length;
        double median = Double.NaN;
        if (combinedLength % 2 == 1) {
            median = findElement(nums1, nums2, combinedLength / 2 + 1);
        } else {
            double medianLeft = findElement(nums1, nums2, combinedLength / 2);
            double medianRight = findElement(nums1, nums2, combinedLength / 2 + 1);
            median = (medianLeft + medianRight) / 2.0;
        }
        return median;
    }

    private double findElement(int[] nums1, int[] nums2, int elementIndex) {
        int lo1 = 0;
        int lo2 = 0;
        while (true) {
            if (lo1 >= nums1.length) {
                return nums2[lo2 + elementIndex - 1];
            }
            if (lo2 >= nums2.length) {
                return nums1[lo1 + elementIndex - 1];
            }
            if (elementIndex == 1) {
                return Math.min(nums1[lo1], nums2[lo2]);
            }
            int halfIndex = elementIndex / 2;
            int element1 = (lo1 + halfIndex - 1) < nums1.length ? nums1[lo1 + halfIndex - 1] : Integer.MAX_VALUE;
            int element2 = (lo2 + halfIndex - 1) < nums2.length ? nums2[lo2 + halfIndex - 1] : Integer.MAX_VALUE;
            if (element1 < element2) {
                lo1 = lo1 + halfIndex;
            } else {
                lo2 = lo2 + halfIndex;
            }
            elementIndex -= halfIndex;
        }
    }
}
