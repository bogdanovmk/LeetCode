/**
 * 33. Search in Rotated Sorted Array<br>
 * Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.<br>
 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).<br>
 * You are given a target value to search. If found in the array return its index, otherwise return -1.<br>
 * You may assume no duplicate exists in the array.<br>
 * @author Maksym Bogdanov
 */
public class Q033_Search_in_Rotated_Sorted_Array {

    public int search(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        if (nums.length == 1) {
            return (nums[0] == target) ? 0 : -1;
        }
        //get pivotIndex position
        int pivotIndex = getPivot(nums, 0, nums.length - 1);
        //check if pivot is our target
        if (nums[pivotIndex] == target) {
            return pivotIndex;
        }
        //make binary search on left part
        int targetIndex = getTargetIndex(nums, 0, pivotIndex - 1, target);
        if (targetIndex == -1) {
            //make binary search on right part
            targetIndex = getTargetIndex(nums, pivotIndex + 1, nums.length - 1, target);
        }
        return targetIndex;

    }

    private int getPivot(int[] nums, int lo, int hi) {
        int[] sortedNums = nums.clone();
        java.util.Arrays.sort(sortedNums);
        int lastElement = nums[nums.length - 1];
        int lastElementIndex = getTargetIndex(sortedNums, 0, nums.length - 1, lastElement);
        int offset = (lastElementIndex + 1) % nums.length;
        return nums.length - 1 - offset;
    }

    private int getTargetIndex(int[] nums, int lo, int hi, int searchElement) {
        int resultIndex = -1;
        if (lo == hi) {
            return (nums[lo] == searchElement) ? lo : -1;
        } else if (hi > lo) {
            int medianIndex = getMedianIndex(lo, hi);
            if (nums[medianIndex] > searchElement) {
                resultIndex = getTargetIndex(nums, lo, medianIndex - 1, searchElement);
            } else if (nums[medianIndex] < searchElement) {
                resultIndex = getTargetIndex(nums, medianIndex + 1, hi, searchElement);
            } else {
                resultIndex = medianIndex;
            }
        }
        return resultIndex;
    }

    private int getMedianIndex(int lo, int hi) {
        return lo + (hi - lo) / 2;
    }
}
