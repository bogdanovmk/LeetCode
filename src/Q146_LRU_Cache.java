import java.util.HashMap;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache.
 * It should support the following operations: get and set.<br/>
 * <pre>get(key)</pre>
 * Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.<br/>
 * <pre>set(key, value)</pre>
 * Set or insert the value if the key is not already present.<br/>
 * When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
 * @author Maksym Bogdanov
 */
public class Q146_LRU_Cache {

    private HashMap<Integer, CacheNode> values = null;
    private CacheNode last;
    private CacheNode first;
    private int capacity = 0;

    public Q146_LRU_Cache(int capacity) {
        this.capacity = capacity;
        values = new HashMap<>(capacity);
    }

    public int get(int key) {
        if (!values.isEmpty()) {
            CacheNode cacheNode = values.get(key);
            if (cacheNode != null) {
                promoteCacheNode(cacheNode);
                return cacheNode.value;
            }
        }
        return -1;
    }

    public void set(int key, int value) {
        CacheNode newCacheNode = values.get(key);
        if (newCacheNode != null) {
            promoteCacheNode(newCacheNode);
            newCacheNode.key = key;
            newCacheNode.value = value;
        } else if (values.size() == capacity) {
            newCacheNode = last;
            if (last.prev != null) {
                last = last.prev;
                last.next = null;
                newCacheNode.prev = null;
                newCacheNode.next = first;
                first.prev = newCacheNode;
                first = newCacheNode;
            }
            values.remove(newCacheNode.key);
            newCacheNode.key = key;
            newCacheNode.value = value;
        } else {
            newCacheNode = new CacheNode();
            newCacheNode.key = key;
            newCacheNode.value = value;
            if (values.isEmpty()) {
                last = newCacheNode;
                first = newCacheNode;
            } else {
                newCacheNode.next = first;
                first.prev = newCacheNode;
                first = newCacheNode;
            }
        }
        values.put(key, newCacheNode);
    }

    private void promoteCacheNode(CacheNode newCacheNode) {
        if (first != newCacheNode) {
            CacheNode prev = newCacheNode.prev;
            CacheNode next = newCacheNode.next;
            prev.next = next;
            if (next != null) {
                next.prev = prev;
            } else {
                last = prev;
            }
            newCacheNode.prev = null;
            newCacheNode.next = first;
            first.prev = newCacheNode;
            first = newCacheNode;
        }
    }

    private class CacheNode {
        int key;
        int value;
        CacheNode prev;
        CacheNode next;
    }

    public static void main(String... args) {
        /*
        Q146_LRU_Cache solution = new Q146_LRU_Cache(5);
        solution.set(1, 10);
        solution.get(1);
        solution.set(2, 11);
        solution.set(3, 12);
        solution.set(4, 13);
        solution.get(3);
        solution.set(5, 14);
        solution.get(1);
        solution.set(6, 15);
        solution.set(6, 24);
        solution.get(4);
        System.out.println("###############");
        CacheNode current = solution.first;
        while (current != null) {
            System.out.print(current.value);
            System.out.print(" ");
            current = current.next;
        }
        System.out.println();
        System.out.println("###############");
        current = solution.last;
        while (current != null) {
            System.out.print(current.value);
            System.out.print(" ");
            current = current.prev;
        }
        System.out.println();

        Q146_LRU_Cache solution = new Q146_LRU_Cache(1);
        solution.set(2,1);
        solution.get(2);
        solution.set(3,2);
        solution.get(2);
        solution.get(3);
        */
        Q146_LRU_Cache solution = new Q146_LRU_Cache(10);
        solution.set(10,13);
        solution.set(3,17);
        solution.set(6,11);
        solution.set(10,5);
        solution.set(9,10);
        solution.get(13);
        solution.set(2,19);
        solution.get(2);
        solution.get(3);
        solution.set(5,25);
        solution.get(8);
        solution.set(9,22);
        solution.set(5,5);
        solution.set(1,30);
        solution.get(11);
        solution.set(9,12);
        solution.get(7);
        solution.get(5);
        solution.get(8);
        solution.get(9);
        solution.set(4,30);
        solution.set(9,3);
        solution.get(9);
        solution.get(10);
        solution.get(10);
        solution.set(6,14);
        solution.set(3,1);
        solution.get(3);
        solution.set(10,11);
        solution.get(8);
        solution.set(2,14);
        solution.get(1);
        solution.get(5);
        solution.get(4);
        solution.set(11,4);
        solution.set(12,24);
        solution.set(5,18);
        solution.get(13);
        solution.set(7,23);
        solution.get(8);
        solution.get(12);
        solution.set(3,27);
        solution.set(2,12);
        solution.get(5);
        solution.set(2,9);
        solution.set(13,4);
        solution.set(8,18);
        solution.set(1,7);
        solution.get(6);
        solution.set(9,29);
        solution.set(8,21);
        solution.get(5);
        solution.set(6,30);
        solution.set(1,12);
        solution.get(10);
        solution.set(4,15);
        solution.set(7,22);
        solution.set(11,26);
        solution.set(8,17);
        solution.set(9,29);
        solution.get(5);
        solution.set(3,4);
        solution.set(11,30);
        solution.get(12);
        solution.set(4,29);
        solution.get(3);
        solution.get(9);
        solution.get(6);
        solution.set(3,4);
        solution.get(1);
        solution.get(10);
        solution.set(3,29);
        solution.set(10,28);
        solution.set(1,20);
        solution.set(11,13);
        solution.get(3);
        solution.set(3,12);
        solution.set(3,8);
        solution.set(10,9);
        solution.set(3,26);
        solution.get(8);
        solution.get(7);
        solution.get(5);
        solution.set(13,17);
        solution.set(2,27);
        solution.set(11,15);
        solution.get(12);
        solution.set(9,19);
        solution.set(2,15);
        solution.set(3,16);
        solution.get(1);
        solution.set(12,17);
        solution.set(9,1);
        solution.set(6,19);
        solution.get(4);
        solution.get(5);
        solution.get(5);
        solution.set(8,1);
        solution.set(11,7);
        solution.set(5,2);
        solution.set(9,28);
        solution.get(1);
        solution.set(2,2);
        solution.set(7,4);
        solution.set(4,22);
        solution.set(7,24);
        solution.set(9,26);
        solution.set(13,28);
        solution.set(11,26);

    }


}
