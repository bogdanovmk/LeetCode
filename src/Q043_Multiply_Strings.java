/**
 * Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2.<br/>
 * Note:
 * <pre>
 * The length of both num1 and num2 is < 110.
 * Both num1 and num2 contains only digits 0-9.
 * Both num1 and num2 does not contain any leading zero.
 * You must not use any built-in BigInteger library or convert the inputs to integer directly.
 * </pre>
 * @author Maksym Bogdanov
 */
public class Q043_Multiply_Strings {

    public String multiply(String num1, String num2) {
        if (num1.length() == 0 || num2.length() == 0) {
            return "0";
        }
        char[] num1Arr = num1.toCharArray();
        char[] num2Arr = num2.toCharArray();
        int[] multiplications = new int[num1Arr.length + num2Arr.length];

        StringBuilder builder = new StringBuilder(multiplications.length);

        for (int i = num1Arr.length - 1; i >= 0; i--) {
            int elInt1 = num1Arr[i] - '0';
            for (int j = num2Arr.length - 1; j >= 0; j--) {
                int elInt2 = num2Arr[j] - '0';
                multiplications[i + j] = multiplications[i + j] + elInt1 * elInt2;
            }
        }

        int carry = 0;
        for (int k = multiplications.length - 2; k >= 0; k--) {
            carry += multiplications[k];
            int mod = carry % 10;
            carry = carry / 10;
            builder.insert(0, mod);
        }
        builder.insert(0, carry);

        while (builder.length() > 1 && builder.charAt(0) == '0') {
            builder.deleteCharAt(0);
        }

        return builder.toString();
    }

    public static void main(String... args) {
        testMultiplication(9, 8);
        testMultiplication(11, 11);
        testMultiplication(116, 119);
        testMultiplication(67890345, 437876879);
    }

    private static void testMultiplication(long l1, long l2) {
        Q043_Multiply_Strings solution = new Q043_Multiply_Strings();
        System.out.print(" Expected: ");
        System.out.print(l1 * l2);
        System.out.print("; Actual: ");
        System.out.println(solution.multiply(String.valueOf(l1), String.valueOf(l2)));
    }
}
