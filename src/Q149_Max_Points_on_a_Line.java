import java.util.HashMap;
import java.util.Map;

/**
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
 * @author Maksym Bogdanov
 */
public class Q149_Max_Points_on_a_Line {
    public int maxPoints(Point[] points) {
        if (points == null) {
            throw new NullPointerException();
        }
        if (points.length <= 2) {
            return points.length;
        }

        int maxPoints = 0;
        for (int i = 0; i < points.length; i++) {
            Point p1 = points[i];
            int duplicatePts = 1;
            int verticalPts = 0;
            int tmpMaxPoints = 0;
            Map<Double, Integer> counters = new HashMap<>();
            for (int j = i + 1; j < points.length; j++) {
                Point p2 = points[j];
                if (p1.x == p2.x) {
                    if (p1.y == p2.y) {
                        duplicatePts++;
                    } else {
                        verticalPts++;
                    }
                } else {
                    double slope = (p1.y == p2.y) ? 0.0D : (double) (p2.y - p1.y) / (double) (p2.x - p1.x);
                    int counter = incrementAndGetCounter(counters, slope);
                    if (counter > tmpMaxPoints) {
                        tmpMaxPoints = counter;
                    }
                }
            }
            tmpMaxPoints = tmpMaxPoints + duplicatePts;
            tmpMaxPoints = Math.max(verticalPts + duplicatePts, tmpMaxPoints);
            maxPoints = Math.max(maxPoints, tmpMaxPoints);
        }

        return maxPoints;
    }

    private int incrementAndGetCounter(Map<Double, Integer> counters, Double key) {
        Integer counter = counters.get(key);
        if (counter == null) {
            counter = 0;
        }
        counter++;
        counters.put(key, counter);
        return counter;
    }

    class Point {
        int x;
        int y;

        Point() {
            x = 0;
            y = 0;
        }

        Point(int a, int b) {
            x = a;
            y = b;
        }
    }
}
