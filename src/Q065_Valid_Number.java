/**
 *
 * Validate if a given string is numeric.<br/>
 * Some examples:<br/>
 * "0" => true<br/>
 * " 0.1 " => true<br/>
 * "abc" => false<br/>
 * "1 a" => false<br/>
 * "2e10" => true<br/>
 * Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.<br/>
 * @author Maksym Bogdanov
 */
public class Q065_Valid_Number {

    public boolean isNumber(String s) {
        if (s == null) {
            return false;
        }
        s = s.trim();
        if (s.length() == 0) {
            return false;
        }
        char[] chars = s.toUpperCase().toCharArray();
        boolean hasDot = false;
        boolean hasSign = false;
        boolean hasENotation = false;
        boolean hasDigit = false;
        int start = 0;
        char ch = chars[start++];
        if (isSign(ch)) {
            hasSign = true;
            if (isNextElementOutOfRange(chars, start - 1)) {
                return false;
            }
        } else if (isDot(ch)) {
            if (isNextElementOutOfRange(chars, start - 1)) {
                return false;
            } else if (isDigit(chars[start++])) {
                hasDot = true;
                hasDigit = true;
            } else {
                return false;
            }
        } else if (!isDigit(ch)) {
            return false;
        } else {
            hasDigit = true;
        }

        for (int i = start; i < chars.length; i++) {
            ch = chars[i];
            if (isDot(ch)) {
                if (hasDot || hasENotation) {
                    return false;
                } else if (isNextElementOutOfRange(chars, i)) {
                    return hasDigit;
                } else if (isDigit(chars[i + 1])) {
                    i++;
                    hasDot = true;
                    if (!hasENotation) {
                        hasDigit = true;
                    }
                } else if (!hasENotation && isENotation(chars[i + 1])) {
                    continue;
                } else {
                    return false;
                }
            } else if (isENotation(ch)) {
                if (hasENotation || isNextElementOutOfRange(chars, i)) {
                    return false;
                } else {
                    if (isSign(chars[i + 1])) {
                        i++;
                        if (isNextElementOutOfRange(chars, i)) {
                            return false;
                        }
                    }
                    hasENotation = true;
                }

            } else if (!isDigit(ch)) {
                return false;
            } else {
                if (!hasENotation) {
                    hasDigit = true;
                }
            }
        }
        return hasDigit;
    }

    public boolean isDigit(char ch) {
        switch (ch) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return true;
            default:
                return false;
        }
    }

    public boolean isDot(char ch) {
        return ch == '.';
    }

    public boolean isSign(char ch) {
        return ch == '+' || ch == '-';
    }

    public boolean isENotation(char ch) {
        return ch == 'E';
    }

    public boolean isNextElementOutOfRange(char[] chars, int currentIndex) {
        return (currentIndex + 1) >= chars.length;
    }
}
