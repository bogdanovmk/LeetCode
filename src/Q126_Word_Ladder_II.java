import java.util.*;

/**
 * Given two words (beginWord and endWord), and a dictionary's word list,
 * find all shortest transformation sequence(s) from beginWord to endWord, such that:<br/>
 * Only one letter can be changed at a time<br/>
 * Each intermediate word must exist in the word list<br/>
 * For example,
 * <p>
 * Given:<br/>
 * beginWord = "hit"<br/>
 * endWord = "cog"<br/>
 * wordList = ["hot","dot","dog","lot","log"]<br/>
 * Return<br/>
 * [<br/>
 * ["hit","hot","dot","dog","cog"],<br/>
 * ["hit","hot","lot","log","cog"]<br/>
 * ]
 * <p/>
 * Note:<br/>
 * All words have the same length.<br/>
 * All words contain only lowercase alphabetic characters.<br/>
 * @author Maksym Bogdanov
 */
public class Q126_Word_Ladder_II {

    public List<List<String>> findLadders(String beginWord, String endWord, Set<String> wordList) {
        List<List<String>> result = new ArrayList<>();
        if (!wordList.isEmpty()) {
            Set<String> dictionary = new HashSet<>();
            dictionary.addAll(wordList);
            dictionary.add(endWord);
            Set<String> processed = new HashSet<String>();
            LinkedList<Node> queue = new LinkedList<Node>();
            queue.add(new Node(null, beginWord, 1));
            int maxLength = Integer.MAX_VALUE;
            int previousLevel = 0;
            while (!queue.isEmpty()) {
                Node node = queue.remove();
                String word = node.word;
                int level = node.level;
                if (word.equals(endWord)) {
                    List<String> target = new LinkedList<>();
                    Node current = node;
                    while (current != null) {
                        target.add(0, current.word);
                        current = current.root;
                    }
                    if (maxLength >= target.size()) {
                        result.add(target);
                        maxLength = target.size();
                    }
                    continue;
                }
                if (previousLevel < level) {
                    dictionary.removeAll(processed);
                }

                previousLevel = level;
                char[] chars = word.toCharArray();
                for (int i = 0; i < chars.length; i++) {
                    char original = chars[i];
                    char ch = 'a';
                    while (ch <= 'z') {
                        chars[i] = ch;
                        ch++;
                        String newWord = String.valueOf(chars);
                        if (dictionary.contains(newWord)) {
                            queue.add(new Node(node, newWord, level + 1));
                            processed.add(newWord);
                        }
                    }
                    chars[i] = original;
                }
            }
        }
        return result;
    }

    private class Node {
        Node root;
        String word;
        int level;
        public Node(Node root, String word, int level) {
            this.root = root;
            this.word = word;
            this.level = level;
        }
    }

}
